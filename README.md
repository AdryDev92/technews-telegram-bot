# technews-telegram-bot🤖

Telegram bot to receive last technology news in your groups or channels.

**This project is completly open source under MIT license** ⚖️.

![version](https://img.shields.io/badge/version-0.1.0-brightgreen.svg) ![build](https://img.shields.io/badge/build-0701-brightgreen.svg)

## How to contribute✍🏻
If you want contribute in this project, you can `fork` the repo and propose your change in a `pull request`.

## Contact me📩
Send me an email to: adrianmprados@gmail.com
